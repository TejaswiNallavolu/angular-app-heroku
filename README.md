# Class-Reminder-App [![Codacy Badge](https://app.codacy.com/project/badge/Grade/41ca830b8b6a4dd591510ea9141fa352)](https://www.codacy.com/gh/TejaswiNallavolu/angular-app-heroku/dashboard?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=TejaswiNallavolu/angular-app-heroku&amp;utm_campaign=Badge_Grade)

-  **Class-Reminder App** makes it easy to stay connected to your school community whether you're in the classroom, at home or anywhere in between. Communicate in real-time on any device.
-  We are planning to develop this app using technologies based on c# with **DotNet** as back-end and **Angular** as front-end.
-  As **C#** is brought up by Microsoft, it is most popularly used for the development of Windows desktop applications.
-  **Angular** is an open-source JavaScript framework most suited to develop this application.

## External Requirements
-  Visual Studio Code  
-  Visual Studio  
-  MySql for the database
-  Heroku
-  Somee

## My Team   
| Name      | Role | Email     |
| :---        |    :----:   | :---          
| [Tejaswi Reddy Nallavolu](https://github.com/TejaswiNallavolu)      | Front-End-Developer      | S542271@nwmissouri.edu   |
| [Priyanka Thambabathula](https://github.com/Priyanka1818)   | Back-End-Developer        | S542414@nwmissouri.edu      |

## Useful Links
-  [Wiki](https://github.com/TejaswiNallavolu/angular-app-heroku/wiki)   
-  [Issues](https://github.com/TejaswiNallavolu/angular-app-heroku/issues)
-  [Project Board](https://github.com/TejaswiNallavolu/angular-app-heroku/projects/1)
-  [Back-end Repo](https://github.com/TejaswiNallavolu/BackEnd-ClassReminderApp)
-  [RFP](https://github.com/harshakurra123/ClassRemainder)
-  [Proposal](https://github.com/TejaswiNallavolu/proposal)
